package aisco.donation.pgateway;

import java.util.HashMap;

import javax.persistence.Entity;
import javax.persistence.Table;

import aisco.donation.core.DonationDecorator;
import aisco.donation.core.DonationComponent;

@Entity(name="donation_pgateway")
@Table(name="donation_pgateway")
public class DonationImpl extends DonationDecorator {
	
	protected String paymentId;
	protected String status;
	protected String requestedDate;
	public DonationImpl (DonationComponent record, String paymentId, String status, String requestedDate) {
		super(record);
		this.paymentId = paymentId;
		this.status = status;
		this.requestedDate = requestedDate;
	}
	
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	
	public String getPaymentId() {
		return this.paymentId;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return this.status;
	}
	
	public void setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
	}
	
	public String getRequestedDate() {
		return this.requestedDate;
	}
	
	public HashMap<String,Object> toHashMap() {
		HashMap<String,Object> recordHashMap = record.toHashMap();
		recordHashMap.put("paymentId", getPaymentId());
		recordHashMap.put("status", getStatus());
		recordHashMap.put("requestedDate", getRequestedDate());
		return recordHashMap;
	}
}