package aisco.donation.pgateway;

import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import aisco.donation.DonationFactory;
import aisco.donation.core.Donation;
import aisco.donation.core.DonationResourceDecorator;
import aisco.donation.core.DonationResourceComponent;
//import aisco.donation.core.DonationImpl;

import paymentgateway.payment.PaymentFactory;
import paymentgateway.payment.PaymentResourceFactory;
import paymentgateway.payment.core.PaymentResource;
import paymentgateway.payment.core.Payment;
import paymentgateway.payment.core.PaymentResourceImpl;
import paymentgateway.payment.paymentrouting.PaymentRoutingRecipient;

public class DonationResourceImpl extends DonationResourceDecorator {
	
	protected PaymentResource virtualAccountResource;
	protected PaymentResource retailOutletResource;
	protected PaymentResource invoiceResource;
	protected PaymentResource paymentlinkResource;
	protected PaymentResource debitResource;
	protected PaymentResource ewalletResource;
	protected PaymentResource creditResource;
	protected PaymentResource paymentroutingResource;
	public DonationResourceImpl (DonationResourceComponent record) {
		super(record);
		this.virtualAccountResource = PaymentResourceFactory.createPaymentResource(
				"paymentgateway.payment.virtualaccount.PaymentResourceImpl",
				new PaymentResourceImpl()
				);
		this.retailOutletResource = PaymentResourceFactory.createPaymentResource(
				"paymentgateway.payment.retailoutlet.PaymentResourceImpl",
				new PaymentResourceImpl()
				);
		this.invoiceResource = PaymentResourceFactory.createPaymentResource(
				"paymentgateway.payment.invoice.PaymentResourceImpl",
				new PaymentResourceImpl()
				);
		this.paymentlinkResource = PaymentResourceFactory.createPaymentResource(
				"paymentgateway.payment.paymentlink.PaymentResourceImpl",
				new PaymentResourceImpl()
				);
		this.debitResource = PaymentResourceFactory.createPaymentResource(
				"paymentgateway.payment.debitcard.PaymentResourceImpl",
				new PaymentResourceImpl()
				);
		this.ewalletResource = PaymentResourceFactory.createPaymentResource(
				"paymentgateway.payment.ewallet.PaymentResourceImpl",
				new PaymentResourceImpl()
				);
		this.creditResource = PaymentResourceFactory.createPaymentResource(
				"paymentgateway.payment.creditcard.PaymentResourceImpl",
				new PaymentResourceImpl()
				);
		this.paymentroutingResource = PaymentResourceFactory.createPaymentResource(
				"paymentgateway.payment.paymentrouting.PaymentResourceImpl",
				new PaymentResourceImpl()
				);
	}
	
	@Route(url="call/donation-pgateway/create")
	public HashMap<String,Object> requestDonation(VMJExchange vmjExchange) {
		//if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
		
		HashMap<String,Object> componentExchange = new HashMap<String,Object>();
		componentExchange.put("name", (String) vmjExchange.getRequestBodyForm("name"));
		componentExchange.put("email", (String) vmjExchange.getRequestBodyForm("email"));
		componentExchange.put("phonenumber", (String) vmjExchange.getRequestBodyForm("phonenumber"));
		componentExchange.put("message", (String) vmjExchange.getRequestBodyForm("message"));
		componentExchange.put("paymentmethod", (String) vmjExchange.getRequestBodyForm("paymentmethod"));
		componentExchange.put("idprogram", (String) vmjExchange.getRequestBodyForm("idprogram"));
		componentExchange.put("transferdate", "");
		componentExchange.put("requestedDate", LocalDateTime.now().toLocalDate().toString());
		componentExchange.put("amount", (String) vmjExchange.getRequestBodyForm("amount"));
		HashMap<String,Object> paymentDonation = createPaymentDonation(componentExchange);
		Donation donation = (Donation) paymentDonation.get("donation");
		donationDao.saveObject(donation);
		return paymentDonation;
	}
	
	@Route(url="call/donation-pgateway/create-invoice")
	public HashMap<String,Object> createInvoice(VMJExchange vmjExchange) {
		//if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
		
		HashMap<String,Object> componentExchange = new HashMap<String,Object>();
		componentExchange.put("name", (String) vmjExchange.getRequestBodyForm("name"));
		componentExchange.put("email", (String) vmjExchange.getRequestBodyForm("email"));
		componentExchange.put("phonenumber", (String) vmjExchange.getRequestBodyForm("phonenumber"));
		componentExchange.put("message", (String) vmjExchange.getRequestBodyForm("message"));
		componentExchange.put("paymentmethod", "invoice");
		componentExchange.put("idprogram", (String) vmjExchange.getRequestBodyForm("idprogram"));
		componentExchange.put("transferdate", "");
		componentExchange.put("requestedDate", LocalDateTime.now().toLocalDate().toString());
		componentExchange.put("amount", (String) vmjExchange.getRequestBodyForm("amount"));
		HashMap<String,Object> paymentDonation = viaInvoice(componentExchange);
		//Donation donation = (Donation) paymentDonation.get("donation");
		//donationDao.saveObject(donation);
		return paymentDonation;
	}
	
	@Route(url="call/donation-pgateway/create-retailoutlet")
	public HashMap<String,Object> createRetailOutlet(VMJExchange vmjExchange) {
		//if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
		
		HashMap<String,Object> componentExchange = new HashMap<String,Object>();
		componentExchange.put("name", (String) vmjExchange.getRequestBodyForm("name"));
		componentExchange.put("email", (String) vmjExchange.getRequestBodyForm("email"));
		componentExchange.put("phonenumber", (String) vmjExchange.getRequestBodyForm("phonenumber"));
		componentExchange.put("message", (String) vmjExchange.getRequestBodyForm("message"));
		componentExchange.put("paymentmethod", (String) vmjExchange.getRequestBodyForm("paymentmethod"));
		componentExchange.put("idprogram", (String) vmjExchange.getRequestBodyForm("idprogram"));
		componentExchange.put("transferdate", "");
		componentExchange.put("requestedDate", LocalDateTime.now().toLocalDate().toString());
		componentExchange.put("amount", (String) vmjExchange.getRequestBodyForm("amount"));
		HashMap<String,Object> paymentDonation = viaRetailOutlet(componentExchange);
		//Donation donation = (Donation) paymentDonation.get("donation");
		//donationDao.saveObject(donation);
		return paymentDonation;
	}
	
	@Route(url="call/donation-pgateway/create-virtualaccount")
	public HashMap<String,Object> createVirtualAccount(VMJExchange vmjExchange) {
		//if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
		
		HashMap<String,Object> componentExchange = new HashMap<String,Object>();
		componentExchange.put("name", (String) vmjExchange.getRequestBodyForm("name"));
		componentExchange.put("email", (String) vmjExchange.getRequestBodyForm("email"));
		componentExchange.put("phonenumber", (String) vmjExchange.getRequestBodyForm("phonenumber"));
		componentExchange.put("message", (String) vmjExchange.getRequestBodyForm("message"));
		componentExchange.put("paymentmethod", (String) vmjExchange.getRequestBodyForm("paymentmethod"));
		componentExchange.put("idprogram", (String) vmjExchange.getRequestBodyForm("idprogram"));
		componentExchange.put("transferdate", "");
		componentExchange.put("requestedDate", LocalDateTime.now().toLocalDate().toString());
		componentExchange.put("amount", (String) vmjExchange.getRequestBodyForm("amount"));
		HashMap<String,Object> paymentDonation = viaVirtualAccount(componentExchange);
		//Donation donation = (Donation) paymentDonation.get("donation");
		//donationDao.saveObject(donation);
		return paymentDonation;
	}
	
	@Route(url="call/donation-pgateway/create-paymentlink")
	public HashMap<String,Object> createPaymentLink(VMJExchange vmjExchange) {
		//if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
		
		HashMap<String,Object> componentExchange = new HashMap<String,Object>();
		componentExchange.put("name", (String) vmjExchange.getRequestBodyForm("name"));
		componentExchange.put("email", (String) vmjExchange.getRequestBodyForm("email"));
		componentExchange.put("phonenumber", (String) vmjExchange.getRequestBodyForm("phonenumber"));
		componentExchange.put("message", (String) vmjExchange.getRequestBodyForm("message"));
		//componentExchange.put("paymentmethod", (String) vmjExchange.getRequestBodyForm("paymentmethod"));
		componentExchange.put("idprogram", (String) vmjExchange.getRequestBodyForm("idprogram"));
		componentExchange.put("transferdate", "");
		componentExchange.put("requestedDate", LocalDateTime.now().toLocalDate().toString());
		componentExchange.put("amount", (String) vmjExchange.getRequestBodyForm("amount"));
		HashMap<String,Object> paymentDonation = viaPaymentLink(componentExchange);
		//Donation donation = (Donation) paymentDonation.get("donation");
		//donationDao.saveObject(donation);
		return paymentDonation;
	}
	
	@Route(url="call/donation-pgateway/create-debit")
	public HashMap<String,Object> createDebit(VMJExchange vmjExchange) {
		//if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
		
		HashMap<String,Object> componentExchange = new HashMap<String,Object>();
		componentExchange.put("name", (String) vmjExchange.getRequestBodyForm("name"));
		componentExchange.put("email", (String) vmjExchange.getRequestBodyForm("email"));
		componentExchange.put("phonenumber", (String) vmjExchange.getRequestBodyForm("phonenumber"));
		componentExchange.put("message", (String) vmjExchange.getRequestBodyForm("message"));
		componentExchange.put("paymentmethod", (String) vmjExchange.getRequestBodyForm("paymentmethod"));
		componentExchange.put("idprogram", (String) vmjExchange.getRequestBodyForm("idprogram"));
		componentExchange.put("transferdate", "");
		componentExchange.put("requestedDate", LocalDateTime.now().toLocalDate().toString());
		componentExchange.put("amount", (String) vmjExchange.getRequestBodyForm("amount"));
		HashMap<String,Object> paymentDonation = viaDebit(componentExchange);
		//Donation donation = (Donation) paymentDonation.get("donation");
		//donationDao.saveObject(donation);
		return paymentDonation;
	}
	
	@Route(url="call/donation-pgateway/create-ewallet")
	public HashMap<String,Object> createEwallet(VMJExchange vmjExchange) {
		//if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
		
		HashMap<String,Object> componentExchange = new HashMap<String,Object>();
		componentExchange.put("name", (String) vmjExchange.getRequestBodyForm("name"));
		componentExchange.put("email", (String) vmjExchange.getRequestBodyForm("email"));
		componentExchange.put("phonenumber", (String) vmjExchange.getRequestBodyForm("phonenumber"));
		componentExchange.put("message", (String) vmjExchange.getRequestBodyForm("message"));
		componentExchange.put("paymentmethod", (String) vmjExchange.getRequestBodyForm("paymentmethod"));
		componentExchange.put("idprogram", (String) vmjExchange.getRequestBodyForm("idprogram"));
		componentExchange.put("transferdate", "");
		componentExchange.put("requestedDate", LocalDateTime.now().toLocalDate().toString());
		componentExchange.put("amount", (String) vmjExchange.getRequestBodyForm("amount"));
		HashMap<String,Object> paymentDonation = viaEwallet(componentExchange);
		//Donation donation = (Donation) paymentDonation.get("donation");
		//donationDao.saveObject(donation);
		return paymentDonation;
	}
	
	@Route(url="call/donation-pgateway/create-credit")
	public HashMap<String,Object> createCredit(VMJExchange vmjExchange) {
		//if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
		
		HashMap<String,Object> componentExchange = new HashMap<String,Object>();
		componentExchange.put("name", (String) vmjExchange.getRequestBodyForm("name"));
		componentExchange.put("email", (String) vmjExchange.getRequestBodyForm("email"));
		componentExchange.put("phonenumber", (String) vmjExchange.getRequestBodyForm("phonenumber"));
		componentExchange.put("message", (String) vmjExchange.getRequestBodyForm("message"));
		//componentExchange.put("paymentmethod", (String) vmjExchange.getRequestBodyForm("paymentmethod"));
		componentExchange.put("idprogram", (String) vmjExchange.getRequestBodyForm("idprogram"));
		componentExchange.put("transferdate", "");
		componentExchange.put("requestedDate", LocalDateTime.now().toLocalDate().toString());
		componentExchange.put("amount", (String) vmjExchange.getRequestBodyForm("amount"));
		componentExchange.put("idToken",  (String) vmjExchange.getRequestBodyForm("idToken"));
		HashMap<String,Object> paymentDonation = viaCredit(componentExchange);
		//Donation donation = (Donation) paymentDonation.get("donation");
		//donationDao.saveObject(donation);
		return paymentDonation;
	}
	
	@Route(url="call/donation-pgateway/create-paymentrouting")
	public HashMap<String,Object> createPaymentRouting(VMJExchange vmjExchange) {
		//if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
		
		HashMap<String,Object> componentExchange = new HashMap<String,Object>();
		componentExchange.put("name", (String) vmjExchange.getRequestBodyForm("name"));
		componentExchange.put("email", (String) vmjExchange.getRequestBodyForm("email"));
		componentExchange.put("phonenumber", (String) vmjExchange.getRequestBodyForm("phonenumber"));
		componentExchange.put("message", (String) vmjExchange.getRequestBodyForm("message"));
		//componentExchange.put("paymentmethod", (String) vmjExchange.getRequestBodyForm("paymentmethod"));
		componentExchange.put("idprogram", (String) vmjExchange.getRequestBodyForm("idprogram"));
		componentExchange.put("transferdate", "");
		componentExchange.put("requestedDate", LocalDateTime.now().toLocalDate().toString());
		componentExchange.put("amount", (String) vmjExchange.getRequestBodyForm("amount"));
		HashMap<String,Object> paymentDonation = viaPaymentRouting(componentExchange);
		//Donation donation = (Donation) paymentDonation.get("donation");
		//donationDao.saveObject(donation);
		return paymentDonation;
	}
	
	private HashMap<String,Object> viaPaymentRouting(HashMap<String,Object> componentExchange) {
		Donation donation = record.createDonation(componentExchange);
		System.out.println("DONATION CREATION SUCCEED");
		String idTransaction = "paymentrouting-" + donation.getId();
		int amount = Integer.parseInt(String.valueOf(donation.getAmount()));
		
		HashMap<String,Object> paymentPayload = new HashMap<String,Object>();
		paymentPayload.put("idTransaction", idTransaction);
		paymentPayload.put("amount", amount);
		
		paymentPayload.put("paymentMethods", "VA");
		paymentPayload.put("sourceOfFunds", "002");
		PaymentRoutingRecipient recipient = new PaymentRoutingRecipient();
		recipient.setRecipient_account("1234567890");
		recipient.setRecipient_bank("014");
		recipient.setRecipient_amount(amount);
		recipient.setRecipient_email("recipient_bca@gmail.com");
		recipient.setRecipient_note("merchant bisakita");
		List<PaymentRoutingRecipient> routings = new ArrayList<>();
		routings.add(recipient);
		paymentPayload.put("routings", routings);
		
		Payment payment = paymentroutingResource.createPayment(paymentPayload);
		HashMap<String,Object> paymentMap = payment.toHashMap();
		
		Donation donationPaymentRouting = DonationFactory.createDonation(
				"aisco.donation.pgateway.DonationImpl",
				donation,
				idTransaction,
				"PENDING",
				componentExchange.get("requestedDate")
				);
		
		HashMap<String,Object> result = new HashMap<String,Object>();
		//result.put("donation", donationPaymentRouting);
		result.put("paymentCheckoutUrl", paymentMap.get("paymentCheckoutUrl"));
		return result;
	}
	
	private HashMap<String,Object> viaCredit(HashMap<String,Object> componentExchange) {
		Donation donation = record.createDonation(componentExchange);
		System.out.println("DONATION CREATION SUCCEED");
		String idTransaction = "credit-" + donation.getId();
		
		HashMap<String,Object> paymentPayload = new HashMap<String,Object>();
		paymentPayload.put("idTransaction", idTransaction);
		paymentPayload.put("amount", Integer.parseInt(String.valueOf(donation.getAmount())));
		String idToken = (String) componentExchange.get("idToken");
		paymentPayload.put("idToken", idToken);
		Payment payment = creditResource.createPayment(paymentPayload);
		HashMap<String,Object> paymentMap = payment.toHashMap();
		
		Donation donationCredit = DonationFactory.createDonation(
				"aisco.donation.pgateway.DonationImpl",
				donation,
				idTransaction,
				"PENDING",
				componentExchange.get("requestedDate")
				);
		
		HashMap<String,Object> result = new HashMap<String,Object>();
		//result.put("donation", donationCredit);
		result.put("creditCardUrl", paymentMap.get("creditCardUrl"));
		return result;
	}
	
	private HashMap<String,Object> viaEwallet(HashMap<String,Object> componentExchange) {
		Donation donation = record.createDonation(componentExchange);
		System.out.println("DONATION CREATION SUCCEED");
		String idTransaction = "ewallet-" + donation.getId();
		
		HashMap<String,Object> paymentPayload = new HashMap<String,Object>();
		paymentPayload.put("idTransaction", idTransaction);
		paymentPayload.put("amount", Integer.parseInt(String.valueOf(donation.getAmount())));
		String eWalletType = ((String) componentExchange.get("paymentmethod")).split("-")[1];
		paymentPayload.put("eWalletType", eWalletType);
		System.out.println("Payload");
		System.out.println(paymentPayload);
		Payment payment = ewalletResource.createPayment(paymentPayload);
		HashMap<String,Object> paymentMap = payment.toHashMap();
		System.out.println("Payment Map");
		System.out.println(paymentMap);
		
		Donation donationEwallet = DonationFactory.createDonation(
				"aisco.donation.pgateway.DonationImpl",
				donation,
				idTransaction,
				"PENDING",
				componentExchange.get("requestedDate")
				);
		
		HashMap<String,Object> result = new HashMap<String,Object>();
		//result.put("donation", donationEwallet);
		result.put("eWalletUrl", paymentMap.get("eWalletUrl"));
		return result;
	}
	
	private HashMap<String,Object> viaDebit(HashMap<String,Object> componentExchange) {
		Donation donation = record.createDonation(componentExchange);
		System.out.println("DONATION CREATION SUCCEED");
		String idTransaction = "debit-" + donation.getId();
		
		HashMap<String,Object> paymentPayload = new HashMap<String,Object>();
		paymentPayload.put("idTransaction", idTransaction);
		paymentPayload.put("amount", Integer.parseInt(String.valueOf(donation.getAmount())));
		String bankCode = ((String) componentExchange.get("paymentmethod")).split("-")[1];
		paymentPayload.put("bankCode", bankCode);
		Payment payment = debitResource.createPayment(paymentPayload);
		HashMap<String,Object> paymentMap = payment.toHashMap();
		
		Donation donationDebit = DonationFactory.createDonation(
				"aisco.donation.pgateway.DonationImpl",
				donation,
				idTransaction,
				"PENDING",
				componentExchange.get("requestedDate")
				);
		
		HashMap<String,Object> result = new HashMap<String,Object>();
		//result.put("donation", donationDebit);
		result.put("directDebitUrl", paymentMap.get("directDebitUrl"));
		return result;
	}
	
	private HashMap<String,Object> viaVirtualAccount(HashMap<String,Object> componentExchange) {
		Donation donation = record.createDonation(componentExchange);
		System.out.println("DONATION CREATION SUCCEED");
		String idTransaction = "virtualacccount-" + donation.getId();
		
		HashMap<String,Object> paymentPayload = new HashMap<String,Object>();
		paymentPayload.put("idTransaction", idTransaction);
		paymentPayload.put("amount", Integer.parseInt(String.valueOf(donation.getAmount())));
		paymentPayload.put("isOpenVA", false);
		String bankCode = ((String) componentExchange.get("paymentmethod")).split("-")[1];
		paymentPayload.put("bankCode", bankCode);
		Payment payment = virtualAccountResource.createPayment(paymentPayload);
		HashMap<String,Object> paymentMap = payment.toHashMap();
		
		Donation donationVA = DonationFactory.createDonation(
				"aisco.donation.pgateway.DonationImpl",
				donation,
				idTransaction,
				"PENDING",
				componentExchange.get("requestedDate")
				);
		
		HashMap<String,Object> result = new HashMap<String,Object>();
		//result.put("donation", donationVA);
		result.put("vaAccountNumber", paymentMap.get("vaAccountNumber"));
		return result;
	}
	
	private HashMap<String,Object> viaPaymentLink(HashMap<String,Object> componentExchange) {
		Donation donation = record.createDonation(componentExchange);
		System.out.println("DONATION CREATION SUCCEED");
		String idTransaction = "paymentlink-" + donation.getId();
		
		HashMap<String,Object> paymentPayload = new HashMap<String,Object>();
		paymentPayload.put("idTransaction", idTransaction);
		paymentPayload.put("amount", Integer.parseInt(String.valueOf(donation.getAmount())));
		
		Payment payment = paymentlinkResource.createPayment(paymentPayload);
		HashMap<String,Object> paymentMap = payment.toHashMap();
		
		Donation donationPaymentLink = DonationFactory.createDonation(
				"aisco.donation.pgateway.DonationImpl",
				donation,
				idTransaction,
				"PENDING",
				componentExchange.get("requestedDate")
				);
		
		HashMap<String,Object> result = new HashMap<String,Object>();
		//result.put("donation", donationPaymentLink);
		result.put("paymentLink", paymentMap.get("paymentLink"));
		return result;
	}
	
	private HashMap<String,Object> viaRetailOutlet(HashMap<String,Object> componentExchange) {
		Donation donation = record.createDonation(componentExchange);
		System.out.println("DONATION CREATION SUCCEED");
		String idTransaction = "retailoutlet-" + donation.getId();
		
		HashMap<String,Object> paymentPayload = new HashMap<String,Object>();
		paymentPayload.put("idTransaction", idTransaction);
		paymentPayload.put("amount", Integer.parseInt(String.valueOf(donation.getAmount())));
		String retailOutlet = ((String) componentExchange.get("paymentmethod")).split("-")[1];
		paymentPayload.put("retailOutlet", retailOutlet);
		Payment payment = retailOutletResource.createPayment(paymentPayload);
		HashMap<String,Object> paymentMap = payment.toHashMap();
		
		Donation donationRetailOutlet = DonationFactory.createDonation(
				"aisco.donation.pgateway.DonationImpl",
				donation,
				idTransaction,
				"PENDING",
				componentExchange.get("requestedDate")
				);
		
		HashMap<String,Object> result = new HashMap<String,Object>();
		//result.put("donation", donationRetailOutlet);
		result.put("retailPaymentCode", paymentMap.get("retailPaymentCode"));
		return result;
	}
	
	private HashMap<String,Object> viaInvoice(HashMap<String,Object> componentExchange) {
		Donation donation = record.createDonation(componentExchange);
		System.out.println("DONATION CREATION SUCCEED");
		String idTransaction = "invoice-" + donation.getId();
		
		HashMap<String,Object> paymentPayload = new HashMap<String,Object>();
		paymentPayload.put("idTransaction", idTransaction);
		paymentPayload.put("amount", Integer.parseInt(String.valueOf(donation.getAmount())));
		Payment payment = invoiceResource.createPayment(paymentPayload);
		HashMap<String,Object> paymentMap = payment.toHashMap();
		
		Donation donationInvoice = DonationFactory.createDonation(
				"aisco.donation.pgateway.DonationImpl",
				donation,
				idTransaction,
				"PENDING",
				componentExchange.get("requestedDate")
				);
		
		HashMap<String,Object> result = new HashMap<String,Object>();
		//result.put("donation", donationInvoice);
		result.put("transactionToken", paymentMap.get("transactionToken"));
		return result;
	}
	
	// for midtrans api call for paid payments
	@Route(url="webhooks/midtrans")
	public HashMap<String,Object> midtransWebhook(VMJExchange vmjExchange) {
		//if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
		
		System.out.println("NEW WEBHOOK REQUEST");
		String idTransaction = (String) vmjExchange.getRequestBodyForm("transaction_id");
		String status = (String) vmjExchange.getRequestBodyForm("transaction_status");
		String date = (String) vmjExchange.getRequestBodyForm("transaction_time");
		System.out.println(idTransaction);
		System.out.println(status);
		System.out.println(date);
		if(status.equals("settlement") || status.equals("capture")) {
			HashMap<String,Object> validationPayload =  new HashMap<String,Object>();
			validationPayload.put("idTransaction", idTransaction);
			validationPayload.put("date", date);
			validateDonation(validationPayload);
		}
		return null;
	}
	
	@Route(url="call/donation-pgateway/test")
	public HashMap<String,Object> test(VMJExchange vmjExchange) {
		//if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
		
		String idTransaction = (String) vmjExchange.getRequestBodyForm("idTransaction");
		int id = Integer.parseInt((idTransaction.split("-"))[1]);
		
		Donation donation = (Donation) donationDao.getObject(id);
		System.out.println(donation.toHashMap());
		
		DonationImpl donationImpl = (DonationImpl) donation;
		System.out.println(donationImpl.toHashMap());
		
		HashMap<String,Object> result = new HashMap<String,Object>();
		result.put("donation", donation.toHashMap());
		result.put("donationImpl", donationImpl.toHashMap());
		return result;
	}
	
	public void validateDonation(HashMap<String,Object> componentExchange) {
		String paymentId = (String) componentExchange.get("idTransaction");
		int id = Integer.parseInt((paymentId.split("-"))[1]);
		String date = (String) componentExchange.get("date");
		Donation donation = donationDao.getObject(id);
		donation.setDate(date);
		donationDao.updateObject(donation);
		System.out.println("Saved Donation");
		System.out.println(donation.toHashMap());
		DonationImpl donationPaymentGateway = (DonationImpl) donation;
		donationPaymentGateway.setStatus("PAID");
		donationDao.updateObject(donationPaymentGateway);
		System.out.println("Saved Donation Payment Gateway");
		System.out.println(donationPaymentGateway.toHashMap());
	}
	
	private HashMap<String,Object> createPaymentDonation(HashMap<String,Object> componentExchange) {
        String paymentMethod = (String) componentExchange.get("paymentmethod");
        if(paymentMethod.contains("virtualaccount")) {
        	return viaVirtualAccount(componentExchange);
        }
        
        if(paymentMethod.contains("retailoutlet")) {
        	return viaRetailOutlet(componentExchange);
        }
        
        return viaInvoice(componentExchange);
    }
	
	
	// methods from components ======================================================
    public List<HashMap<String,Object>> saveDonation(VMJExchange vmjExchange) {
        return record.saveDonation(vmjExchange);
    }

    public Donation createDonation(VMJExchange vmjExchange) {
        return record.createDonation(vmjExchange);
    }
    
    public Donation createDonation(VMJExchange vmjExchange, int id) {
        return record.createDonation(vmjExchange, id);
    }

    public HashMap<String, Object> updateDonation(VMJExchange vmjExchange) {
        return record.updateDonation(vmjExchange);
    }

    public HashMap<String, Object> getDonation(VMJExchange vmjExchange) {
        return record.getDonation(vmjExchange);
    }

    public List<HashMap<String,Object>> getAllDonation(VMJExchange vmjExchange) {
        return record.getAllDonation(vmjExchange);
    }

     public List<HashMap<String,Object>> transformDonationListToHashMap(List<Donation> financialReportList) {
         return record.transformDonationListToHashMap(financialReportList);
     }

    public List<HashMap<String,Object>> deleteDonation(VMJExchange vmjExchange) {
        return record.deleteDonation(vmjExchange);
    }
}