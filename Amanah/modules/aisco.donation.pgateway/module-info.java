module aisco.donation.pgateway {
	requires aisco.donation.core;
	exports aisco.donation.pgateway;

	// https://stackoverflow.com/questions/46488346/error32-13-error-cannot-access-referenceable-class-file-for-javax-naming-re/50568217
	requires java.naming;
	requires vmj.hibernate.integrator;
	requires vmj.routing.route;
	requires prices.auth.vmj;
	
	requires paymentgateway.payment.core;
	requires paymentgateway.payment.paymentrouting;
	    
	opens aisco.donation.pgateway to org.hibernate.orm.core, gson;
}