import LandingPage from './containers/LandingPage'
import AdminPage from './containers/AdminPage'
import LoginPage from './containers/LoginPage'
import RegisterPage from './containers/RegisterPage'
import ForgotPasswordPage from './containers/ForgotPasswordPage'

const commonRoutes = [
  { path: '/appearance', element: <AdminPage /> },
  { path: '/forgot-password', element: <ForgotPasswordPage /> },
  { path: '/register', element: <RegisterPage /> },
  { path: '/login', element: <LoginPage /> },
  { path: '/', element: <LandingPage /> },
]

export default commonRoutes
