import { useRoutes } from 'react-router-dom'

import programRoutes from 'program/routes'
import commonRoutes from 'commons/routes'
import incomeRoutes from 'income/routes'
// import expenseRoutes from 'expense/routes'
// import tahunAnggaranRoutes from 'tahunAnggaran/routes'
// import confirmOfflineDonationRoutes from 'confirmOfflineDonation/routes'
import staticPageRoutes from 'staticPage/routes'

const GlobalRoutes = () => {
  const router = useRoutes([
    ...programRoutes,
    ...incomeRoutes,
    // ...expenseRoutes,
    // ...tahunAnggaranRoutes,
    // ...confirmOfflineDonationRoutes,
    ...staticPageRoutes,
    ...commonRoutes,
  ])

  return router
}

export default GlobalRoutes
