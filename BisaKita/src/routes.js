import { useRoutes } from 'react-router-dom'

import expenseRoutes from 'expense/routes.js'
import arusKasReportRoutes from 'arusKasReport/routes.js'
import incomeRoutes from 'income/routes.js'
import viaPaymentGatewayRoutes from 'viaPaymentGateway/routes.js'
import activityRoutes from 'activity/routes.js'
import commonRoutes from 'commons/routes.js'

const GlobalRoutes = () => {
  const router = useRoutes([
    ...expenseRoutes,
    ...arusKasReportRoutes,
    ...incomeRoutes,
    ...viaPaymentGatewayRoutes,
    ...activityRoutes,
    ...commonRoutes,
  ])

  return router
}

export default GlobalRoutes
