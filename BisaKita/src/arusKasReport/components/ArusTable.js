import React from 'react'
import { Link } from 'react-router-dom'

import { useAuth } from 'commons/auth'
import { Button, TableRow, TableCell } from 'commons/components'

const ArusTable = ({ arusItem }) => {
  const { isAuth } = useAuth()

  return (
    <TableRow distinct={false}>
      {/* Data Binding Arus Table Element*/}
      <TableCell>{arusItem?.name}</TableCell>
      <TableCell>{arusItem?.formattedAmount}</TableCell>
      <TableCell>
        <div class="btn-group gap-2">
          {/* View Element Event Arus Table Element*/}
        </div>
      </TableCell>
    </TableRow>
  )
}

export default ArusTable
