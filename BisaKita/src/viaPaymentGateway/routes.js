import { withAuth } from 'commons/auth'

import DonasiFormPage from './containers/DonasiFormPage'
import DonasiPaymentGatewayPage from './containers/DonasiPaymentGatewayPage'
import DonasiInvoicePage from './containers/DonasiInvoicePage'
import DonasiRetailOutletPage from './containers/DonasiRetailOutletPage'
import DonasiConfirmationPage from './containers/DonasiConfirmationPage'
import DonasiVirtualAccountPage from './containers/DonasiVirtualAccountPage'
import DonasiPaymentLinkPage from './containers/DonasiPaymentLinkPage'
import DonasiDebitPage from './containers/DonasiDebitPage'
import DonasiEwalletPage from './containers/DonasiEwalletPage'
import DonasiCreditPage from './containers/DonasiCreditPage'
import DonasiPaymentRoutingPage from './containers/DonasiPaymentRoutingPage'

const viaPaymentGatewayRoutes = [
  {
    path: '/via-payment-gateway',
    element: <DonasiPaymentGatewayPage />,
  },
  {
    path: '/via-payment-gateway/confirmation',
    element: <DonasiConfirmationPage />,
  },
  {
    path: '/via-payment-gateway/invoice',
    element: <DonasiInvoicePage />,
  },
  {
    path: '/via-payment-gateway/retail-outlet',
    element: <DonasiRetailOutletPage />,
  },
  {
    path: '/via-payment-gateway/va',
    element: <DonasiVirtualAccountPage />,
  },
  {
    path: '/via-payment-gateway/paymentlink',
    element: <DonasiPaymentLinkPage />,
  },
  {
    path: '/via-payment-gateway/debit',
    element: <DonasiDebitPage />,
  },
  {
    path: '/via-payment-gateway/ewallet',
    element: <DonasiEwalletPage />,
  },
  {
    path: '/via-payment-gateway/credit',
    element: <DonasiCreditPage />,
  },
  {
    path: '/via-payment-gateway/paymentrouting',
    element: <DonasiPaymentRoutingPage />,
  },
]

export default viaPaymentGatewayRoutes
