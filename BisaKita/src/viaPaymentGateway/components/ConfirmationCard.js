import React, { useContext } from 'react'

import { Button, Detail, VisualizationAttr } from 'commons/components'

const ConfirmationCard = ({ data }) => {
  return data.transactionToken ? (
    <Detail>
      {/* Data Binding Program Data */}
      <VisualizationAttr label="Mohon selesaikan pembayaran di Midtrans Invoice" />
    </Detail>
  ) : data.retailPaymentCode ? (
    <Detail>
      {/* Data Binding Program Data */}
      <VisualizationAttr
        label="Mohon selesaikan pembayaran di retail outlet dengan kode berikut"
        content={data?.retailPaymentCode}
      />
    </Detail>
  ) : data.vaAccountNumber ? (
    <Detail>
      {/* Data Binding Program Data */}
      <VisualizationAttr
        label="Mohon selesaikan pembayaran dengan transfer ke rekening akun virtual berikut"
        content={data?.vaAccountNumber}
      />
    </Detail>
  ) : data.directDebitUrl ? (
    <Detail>
      {/* Data Binding Program Data */}
      <VisualizationAttr
        label="Mohon selesaikan pembayaran di link berikut"
        content={data?.directDebitUrl}
      />
    </Detail>
  ) : data.paymentLink ? (
    <Detail>
      {/* Data Binding Program Data */}
      <VisualizationAttr
        label="Mohon selesaikan pembayaran di link berikut"
        content={data?.paymentLink}
      />
    </Detail>
  ) : data.eWalletUrl ? (
    <Detail>
      {/* Data Binding Program Data */}
      <VisualizationAttr
        label="Mohon selesaikan pembayaran di link berikut"
        content={data?.eWalletUrl}
      />
    </Detail>
  ) : data.creditCardUrl ? (
    <Detail>
      {/* Data Binding Program Data */}
      <VisualizationAttr
        label="Mohon selesaikan pembayaran di link berikut"
        content={data?.creditCardUrl}
      />
    </Detail>
  ) : data.paymentCheckoutUrl ? (
    <Detail>
      {/* Data Binding Program Data */}
      <VisualizationAttr
        label="Mohon selesaikan pembayaran di link berikut"
        content={data?.paymentCheckoutUrl}
      />
    </Detail>
  ) : (
    <></>
  ) // TODO yg lain
}

export default ConfirmationCard
