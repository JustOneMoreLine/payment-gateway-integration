import React, { useContext } from 'react'
import { Link } from 'react-router-dom'

import { Button, ListItem, VisualizationAttr } from 'commons/components'

const PaymentGatewayCard = ({ paymentgateway }) => {
  return (
    <ListItem>
      {/* Data Binding Program Card Element */}
      <Link
        className="strecthed-link"
        to={`/via-payment-gateway/${paymentgateway.paymentmethod}`}
      >
        <div className="card-body">
          <VisualizationAttr content={paymentgateway?.logoUrl} />
          <VisualizationAttr label={paymentgateway?.name} />
        </div>
      </Link>
    </ListItem>
  )
}

export default PaymentGatewayCard
