import React, { useEffect } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import toast from 'react-hot-toast'
import { Button, Form, SelectionField, InputField } from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import createDebit from '../services/createDebit'

const FormDebit = ({ programs }) => {
  useEffect(() => {}, [])

  const { control, handleSubmit } = useForm()
  const navigate = useNavigate()

  const kirim = data => {
    const cleanData = cleanFormData(data)
    console.log('Data')
    console.log(cleanData.Object)
    console.log(data)
    createDebit({
      ...cleanData,
    })
      .then(({ data: { data } }) => {
        console.log(data)
        navigate('/via-payment-gateway/confirmation', { state: data })
      })
      .catch(error => {
        toast.error(
          error.response?.data?.data?.message ||
            error ||
            'Terjadi kesalahan pada sistem. Harap coba lagi!'
        )
      })
  }

  return (
    <Form title="Donasi Via Direct Debit" onSubmit={handleSubmit(kirim)}>
      <Controller
        name="idprogram"
        control={control}
        render={({ field }) => (
          <SelectionField
            label="Nama Program"
            options={programs}
            placeholder="Pilih nama program"
            {...field}
          />
        )}
      />
      <Controller
        name="amount"
        control={control}
        render={({ field }) => (
          <InputField
            label="Jumlah Donasi"
            placeholder="Masukan nominal donasi"
            type="number"
            {...field}
          />
        )}
      />
      <Controller
        name="paymentmethod"
        control={control}
        render={({ field }) => (
          <SelectionField
            label="Direct Debit"
            placeholder="Pilih direct debit untuk pembayaran"
            options={[{ id: 'debit-danamon_online', name: 'Danamon Online' }]}
            {...field}
          />
        )}
      />
      <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <InputField
            label="Nama Donatur"
            placeholder="Masukan nama donatur"
            {...field}
          />
        )}
      />
      <Controller
        name="email"
        control={control}
        render={({ field }) => (
          <InputField
            label="Email Donatur"
            placeholder="Masukan email donatur"
            {...field}
          />
        )}
      />
      <Controller
        name="phonenumber"
        control={control}
        render={({ field }) => (
          <InputField
            label="No Telepon Donatur"
            placeholder="Masukan nomor telepon donatur"
            {...field}
          />
        )}
      />
      <Controller
        name="message"
        control={control}
        render={({ field }) => (
          <InputField
            label="Keterangan"
            placeholder="Masukan keterangan"
            {...field}
          />
        )}
      />
      <div className="card-actions justify-end">
        <Button type="submit" variant="primary">
          Donasi
        </Button>
      </div>
    </Form>
  )
}

export default FormDebit
