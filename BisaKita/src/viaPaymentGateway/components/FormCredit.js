import React, { useEffect } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import toast from 'react-hot-toast'
import { Button, Form, SelectionField, InputField } from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import createCredit from '../services/createCredit'

const FormCredit = ({ programs }) => {
  useEffect(() => {
    const script =
      'https://api.midtrans.com/v2/assets/js/midtrans-new-3ds.min.js'
    const myMidtransClientKey = 'SB-Mid-client-eIEuR3p7PWwUxmYx'

    let scriptTag = document.createElement('script')
    scriptTag.src = script
    scriptTag.setAttribute('data-client-key', myMidtransClientKey)
    scriptTag.setAttribute('data-environment', 'sandbox')
    scriptTag.setAttribute('id', 'midtrans-script')
    document.body.appendChild(scriptTag)

    const midtransScriptUrl = 'https://app.sandbox.midtrans.com/snap/snap.js'
    //change this according to your client-key

    let scriptTagSnap = document.createElement('script')
    scriptTagSnap.src = midtransScriptUrl
    // optional if you want to set script attribute
    // for example snap.js have data-client-key attribute
    scriptTagSnap.setAttribute('data-client-key', myMidtransClientKey)
    document.body.appendChild(scriptTagSnap)

    return () => {
      document.body.removeChild(scriptTag)
      document.body.removeChild(scriptTagSnap)
    }
  }, [])

  const { control, handleSubmit } = useForm()

  const navigate = useNavigate()

  const kirim = data => {
    const cleanData = cleanFormData(data)
    console.log('Data')
    console.log(cleanData.Object)
    console.log(data)
    var cardData = {
      card_number: data.card_number,
      card_exp_month: data.card_exp_month,
      card_exp_year: data.card_exp_year,
      card_cvv: data.card_cvv,
    }
    var options = {
      onSuccess: function(response) {
        // Success to get card token_id, implement as you wish here
        console.log('Success to get card token_id, response:', response)
        var token_id = response.token_id
        console.log('This is the card token_id:', token_id)
        // Implement sending the token_id to backend to proceed to next step
        const newData = {
          idToken: token_id,
          ...cleanData,
        }
        createCredit({
          ...newData,
        })
          .then(({ data: { data } }) => {
            console.log('Credit')
            console.log(data)
            navigate('/via-payment-gateway/confirmation', { state: data })
          })
          .catch(error => {
            toast.error(
              error.response?.data?.data?.message ||
                error ||
                'Terjadi kesalahan pada sistem. Harap coba lagi!'
            )
          })
      },
      onFailure: function(response) {
        // Fail to get card token_id, implement as you wish here
        console.log('Fail to get card token_id, response:', response)
        // you may want to implement displaying failure message to customer.
        // Also record the error message to your log, so you can review
        // what causing failure on this transaction.
        toast.error('Terjadi kesalahan pada sistem. Harap coba lagi!')
      },
    }
    window.MidtransNew3ds.getCardToken(cardData, options)
  }

  return (
    <Form title="Donasi Via Credit" onSubmit={handleSubmit(kirim)}>
      <Controller
        name="idprogram"
        control={control}
        render={({ field }) => (
          <SelectionField
            label="Nama Program"
            options={programs}
            placeholder="Pilih nama program"
            {...field}
          />
        )}
      />
      <Controller
        name="amount"
        control={control}
        render={({ field }) => (
          <InputField
            label="Jumlah Donasi"
            placeholder="Masukan nominal donasi"
            type="number"
            {...field}
          />
        )}
      />
      <Controller
        name="card_number"
        control={control}
        render={({ field }) => (
          <InputField label="Nomor Kartu" type="number" {...field} />
        )}
      />
      <Controller
        name="card_cvv"
        control={control}
        render={({ field }) => (
          <InputField label="Nomor CVV" type="number" {...field} />
        )}
      />
      <Controller
        name="card_exp_month"
        control={control}
        render={({ field }) => (
          <InputField label="Bulan Exp" type="number" {...field} />
        )}
      />
      <Controller
        name="card_exp_year"
        control={control}
        render={({ field }) => (
          <InputField label="Tahun Exp" type="number" {...field} />
        )}
      />
      <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <InputField
            label="Nama Donatur"
            placeholder="Masukan nama donatur"
            {...field}
          />
        )}
      />
      <Controller
        name="email"
        control={control}
        render={({ field }) => (
          <InputField
            label="Email Donatur"
            placeholder="Masukan email donatur"
            {...field}
          />
        )}
      />
      <Controller
        name="phonenumber"
        control={control}
        render={({ field }) => (
          <InputField
            label="No Telepon Donatur"
            placeholder="Masukan nomor telepon donatur"
            {...field}
          />
        )}
      />
      <Controller
        name="message"
        control={control}
        render={({ field }) => (
          <InputField
            label="Keterangan"
            placeholder="Masukan keterangan"
            {...field}
          />
        )}
      />
      <div className="card-actions justify-end">
        <Button type="submit" variant="primary">
          Donasi
        </Button>
      </div>
    </Form>
  )
}

export default FormCredit
