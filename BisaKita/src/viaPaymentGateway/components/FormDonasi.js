import React, { useEffect } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import toast from 'react-hot-toast'
import { Button, Form, SelectionField, InputField } from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import createDonation from '../services/createDonation'

const FormDonasi = ({ programs }) => {
  useEffect(() => {
    const script =
      'https://api.midtrans.com/v2/assets/js/midtrans-new-3ds.min.js'
    const myMidtransClientKey = 'SB-Mid-client-eIEuR3p7PWwUxmYx'

    let scriptTag = document.createElement('script')
    scriptTag.src = script
    scriptTag.setAttribute('data-client-key', myMidtransClientKey)
    scriptTag.setAttribute('data-environment', 'sandbox')
    scriptTag.setAttribute('id', 'midtrans-script')
    document.body.appendChild(scriptTag)

    const midtransScriptUrl = 'https://app.sandbox.midtrans.com/snap/snap.js'
    //change this according to your client-key

    let scriptTagSnap = document.createElement('script')
    scriptTagSnap.src = midtransScriptUrl
    // optional if you want to set script attribute
    // for example snap.js have data-client-key attribute
    scriptTagSnap.setAttribute('data-client-key', myMidtransClientKey)
    document.body.appendChild(scriptTagSnap)

    return () => {
      document.body.removeChild(scriptTag)
      document.body.removeChild(scriptTagSnap)
    }
  }, [])

  const { control, handleSubmit } = useForm()

  const navigate = useNavigate()

  const kirim = data => {
    const cleanData = cleanFormData(data)
    console.log('Data')
    console.log(cleanData.Object)
    console.log(data)
    createDonation({
      ...cleanData,
    })
      .then(({ data: { data } }) => {
        console.log(data)
        if (data.transactionToken) {
          window.snap.pay(data.transactionToken)
        }
        if (data.vaAccountNumber) {
          console.log('VA Account Number %s', data.vaAccountNumber)
        }
        if (data.retailPaymentCode) {
          console.log('Retail Payment Code: %s', data.retailPaymentCode)
        }
        //navigate(`/donation`)
      })
      .catch(error => {
        toast.error(
          error.response?.data?.data?.message ||
            error ||
            'Terjadi kesalahan pada sistem. Harap coba lagi!'
        )
      })
  }

  return (
    <Form title="Donasi Via Payment Gateway" onSubmit={handleSubmit(kirim)}>
      <Controller
        name="idprogram"
        control={control}
        render={({ field }) => (
          <SelectionField
            label="Nama Program"
            options={programs}
            placeholder="Pilih nama program"
            {...field}
          />
        )}
      />
      <Controller
        name="amount"
        control={control}
        render={({ field }) => (
          <InputField
            label="Jumlah Donasi"
            placeholder="Masukan nominal donasi"
            type="number"
            {...field}
          />
        )}
      />
      <Controller
        name="paymentmethod"
        control={control}
        render={({ field }) => (
          <SelectionField
            label="Metode Pembayaran"
            placeholder="Pilih metode pembayaran"
            options={[
              { id: 'invoice', name: 'Invoice Midtrans' },
              { id: 'paymentlink', name: 'Payment Link' },
              { id: 'virtualaccount-bni', name: 'Transfer BNI' },
              { id: 'virtualaccount-bca', name: 'Tranfer BCA' },
              { id: 'retailoutlet-indomaret', name: 'Gerai Retail: Indomaret' },
              { id: 'retailoutlet-alfamart', name: 'Gerai Retail: Alfamart' },
              { id: 'ewallet-gopay', name: 'GOPAY' },
              { id: 'debit-danamon', name: 'Debit Danamon Online' },
              // TODO credit
            ]}
            {...field}
          />
        )}
      />
      <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <InputField
            label="Nama Donatur"
            placeholder="Masukan nama donatur"
            {...field}
          />
        )}
      />
      <Controller
        name="email"
        control={control}
        render={({ field }) => (
          <InputField
            label="Email Donatur"
            placeholder="Masukan email donatur"
            {...field}
          />
        )}
      />
      <Controller
        name="phonenumber"
        control={control}
        render={({ field }) => (
          <InputField
            label="No Telepon Donatur"
            placeholder="Masukan nomor telepon donatur"
            {...field}
          />
        )}
      />
      <Controller
        name="message"
        control={control}
        render={({ field }) => (
          <InputField
            label="Keterangan"
            placeholder="Masukan keterangan"
            {...field}
          />
        )}
      />
      <div className="card-actions justify-end">
        <Button type="submit" variant="primary">
          Donasi
        </Button>
      </div>
    </Form>
  )
}

export default FormDonasi
