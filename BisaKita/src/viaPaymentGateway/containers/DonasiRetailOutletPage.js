import React, { useEffect, useState } from 'react'

import getProgramListElement from 'activity/services/getProgramListElement'

import FormRetailOutlet from '../components/FormRetailOutlet'

const DonasiRetailOutletPage = props => {
  const [programs, setPrograms] = useState()

  useEffect(() => {
    const fetch = async () => {
      const { data: programsListResponse } = await getProgramListElement()

      setPrograms(programsListResponse.data)
    }
    fetch()
  }, [])

  return programs ? (
    <>
      <FormRetailOutlet {...{ programs }} {...props} />
    </>
  ) : (
    <></>
  )
}

export default DonasiRetailOutletPage
