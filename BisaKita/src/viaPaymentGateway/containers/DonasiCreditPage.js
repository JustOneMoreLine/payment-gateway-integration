import React, { useEffect, useState } from 'react'

import getProgramListElement from 'activity/services/getProgramListElement'

import FormCredit from '../components/FormCredit'

const DonasiCreditPage = props => {
  const [programs, setPrograms] = useState()

  useEffect(() => {
    const fetch = async () => {
      const { data: programsListResponse } = await getProgramListElement()

      setPrograms(programsListResponse.data)
    }
    fetch()
  }, [])

  return programs ? (
    <>
      <FormCredit {...{ programs }} {...props} />
    </>
  ) : (
    <></>
  )
}

export default DonasiCreditPage
