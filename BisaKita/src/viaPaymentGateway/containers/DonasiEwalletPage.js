import React, { useEffect, useState } from 'react'

import getProgramListElement from 'activity/services/getProgramListElement'

import FormEwallet from '../components/FormEwallet'

const DonasiEwalletPage = props => {
  const [programs, setPrograms] = useState()

  useEffect(() => {
    const fetch = async () => {
      const { data: programsListResponse } = await getProgramListElement()

      setPrograms(programsListResponse.data)
    }
    fetch()
  }, [])

  return programs ? (
    <>
      <FormEwallet {...{ programs }} {...props} />
    </>
  ) : (
    <></>
  )
}

export default DonasiEwalletPage
