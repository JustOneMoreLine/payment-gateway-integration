import React, { useEffect, useState } from 'react'

import getProgramListElement from 'activity/services/getProgramListElement'

import FormDonasi from '../components/FormDonasi'

const DonasiFormPage = props => {
  const [programs, setPrograms] = useState()

  useEffect(() => {
    const fetch = async () => {
      const { data: programsListResponse } = await getProgramListElement()

      setPrograms(programsListResponse.data)
    }
    fetch()
  }, [])

  return programs ? (
    <>
      <FormDonasi {...{ programs }} {...props} />
    </>
  ) : (
    <></>
  )
}

export default DonasiFormPage
