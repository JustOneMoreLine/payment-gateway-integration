import React, { useEffect, useState } from 'react'

import getProgramListElement from 'activity/services/getProgramListElement'

import FormPaymentRouting from '../components/FormPaymentRouting'

const DonasiPaymentRoutingPage = props => {
  const [programs, setPrograms] = useState()

  useEffect(() => {
    const fetch = async () => {
      const { data: programsListResponse } = await getProgramListElement()

      setPrograms(programsListResponse.data)
    }
    fetch()
  }, [])

  return programs ? (
    <>
      <FormPaymentRouting {...{ programs }} {...props} />
    </>
  ) : (
    <></>
  )
}

export default DonasiPaymentRoutingPage
