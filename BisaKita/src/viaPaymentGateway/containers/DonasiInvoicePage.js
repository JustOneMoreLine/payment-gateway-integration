import React, { useEffect, useState } from 'react'

import getProgramListElement from 'activity/services/getProgramListElement'

import FormInvoice from '../components/FormInvoice'

const DonasiInvoicePage = props => {
  const [programs, setPrograms] = useState()

  useEffect(() => {
    const fetch = async () => {
      const { data: programsListResponse } = await getProgramListElement()

      setPrograms(programsListResponse.data)
    }
    fetch()
  }, [])

  return programs ? (
    <>
      <FormInvoice {...{ programs }} {...props} />
    </>
  ) : (
    <></>
  )
}

export default DonasiInvoicePage
