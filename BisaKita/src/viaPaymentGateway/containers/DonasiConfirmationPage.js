import React, { useEffect, useState } from 'react'

import ConfirmationCard from '../components/ConfirmationCard'
import { useLocation } from 'react-router-dom'

const DonasiConfirmationPage = props => {
  const { state } = useLocation()
  const [data, setData] = useState()

  useEffect(() => {
    console.log(state)
    setData(state)
    console.log(data)
  }, [])

  return data ? (
    <div className="prose max-w-screen-lg mx-auto p-6">
      <h2>Transaksi Payment Berhasil!</h2>
      <ConfirmationCard {...{ data }} />
    </div>
  ) : (
    <></>
  )
}

export default DonasiConfirmationPage
