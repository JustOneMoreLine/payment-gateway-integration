import React, { useEffect, useState } from 'react'

import getProgramListElement from 'activity/services/getProgramListElement'

import FormPaymentLink from '../components/FormPaymentLink'

const DonasiPaymentLinkPage = props => {
  const [programs, setPrograms] = useState()

  useEffect(() => {
    const fetch = async () => {
      const { data: programsListResponse } = await getProgramListElement()

      setPrograms(programsListResponse.data)
    }
    fetch()
  }, [])

  return programs ? (
    <>
      <FormPaymentLink {...{ programs }} {...props} />
    </>
  ) : (
    <></>
  )
}

export default DonasiPaymentLinkPage
