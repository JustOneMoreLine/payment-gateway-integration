import React, { useEffect, useState } from 'react'

import getProgramListElement from 'activity/services/getProgramListElement'

import FormDebit from '../components/FormDebit'

const DonasiDebitPage = props => {
  const [programs, setPrograms] = useState()

  useEffect(() => {
    const fetch = async () => {
      const { data: programsListResponse } = await getProgramListElement()

      setPrograms(programsListResponse.data)
    }
    fetch()
  }, [])

  return programs ? (
    <>
      <FormDebit {...{ programs }} {...props} />
    </>
  ) : (
    <></>
  )
}

export default DonasiDebitPage
