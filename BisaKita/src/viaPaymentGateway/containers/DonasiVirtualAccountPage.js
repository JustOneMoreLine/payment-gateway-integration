import React, { useEffect, useState } from 'react'

import getProgramListElement from 'activity/services/getProgramListElement'

import FormVirtualAccount from 'viaPaymentGateway/components/FormVirtualAccount'

const DonasiVirtualAccountPage = props => {
  const [programs, setPrograms] = useState()

  useEffect(() => {
    const fetch = async () => {
      const { data: programsListResponse } = await getProgramListElement()

      setPrograms(programsListResponse.data)
    }
    fetch()
  }, [])

  return programs ? (
    <>
      <FormVirtualAccount {...{ programs }} {...props} />
    </>
  ) : (
    <></>
  )
}

export default DonasiVirtualAccountPage
