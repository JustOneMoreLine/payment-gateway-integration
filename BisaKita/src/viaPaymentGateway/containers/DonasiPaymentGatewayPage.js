import React, { useEffect, useState } from 'react'

import getProgramListElement from 'activity/services/getProgramListElement'
import {
  Button,
  Table,
  TableRow,
  TableCell,
  TableBody,
  TableHead,
  List,
} from 'commons/components'
import PaymentGatewayCard from 'viaPaymentGateway/components/PaymentGatewayCard'
import { set } from 'react-hook-form'

const DonasiPaymentGatewayPage = props => {
  const [paymentgateway, setPaymentgateway] = useState()

  useEffect(() => {
    const paymentgateway = [
      {
        paymentmethod: 'invoice',
        name: 'Invoice Midtrans',
        logoUrl: '',
      },
      {
        paymentmethod: 'paymentlink',
        name: 'Payment Link',
        logoUrl: '',
      },
      {
        paymentmethod: 'va',
        name: 'Transfer Virtual Account',
        logoUrl: '',
      },
      {
        paymentmethod: 'retail-outlet',
        name: 'Retail Outlet',
        logoUrl: '',
      },
      {
        paymentmethod: 'ewallet',
        name: 'Ewallet',
        logoUrl: '',
      },
      {
        paymentmethod: 'debit',
        name: 'Direct Debit',
        logoUrl: '',
      },
      {
        paymentmethod: 'credit',
        name: 'Credit',
        logoUrl: '',
      },
      {
        paymentmethod: 'paymentrouting',
        name: 'Payment Routing',
        logoUrl: '',
      },
    ]
    setPaymentgateway(paymentgateway)
    fetch()
  }, [])

  return (
    <div className="mx-auto w-full max-w-screen-xl prose p-6 flex flex-col prose">
      <div className="w-full flex justify-center sm:justify-between items-center mb-4">
        <h2 className="m-0">Via Payment Gateway</h2>
      </div>
      <div className="not-prose">
        <GridView {...{ paymentgateway }} />
      </div>
    </div>
  )
}

const GridView = ({ paymentgateway }) => {
  return (
    <List column="4">
      {paymentgateway &&
        paymentgateway.map(programItem => (
          <PaymentGatewayCard paymentgateway={programItem} />
        ))}
    </List>
  )
}

export default DonasiPaymentGatewayPage
