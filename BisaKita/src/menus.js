const menus = [
  {
    route: '#',
    label: 'Program',
    subMenus: [
      {
        route: '/activity',
        label: 'Aktivitas',
      },
    ],
  },
  {
    route: '#',
    label: 'Laporan Keuangan',
    subMenus: [
      {
        route: '/income',
        label: 'Pemasukan',
      },
      {
        route: '/expense',
        label: 'Pengeluaran',
      },
      {
        route: '/report',
        label: 'Jurnal Keuangan',
        subMenus: [
          {
            route: '#',
            label: 'PSAK45',
            subMenus: [
              {
                route: '/laporan-arus-kas',
                label: 'Laporan Arus Kas',
              },
            ],
          },
        ],
      },
    ],
  },
  {
    route: '#',
    label: 'Donasi',
    subMenus: [
      {
        route: '#',
        label: 'Online',
        subMenus: [
          {
            route: '/via-payment-gateway',
            label: 'Via Payment Gateway',
          },
        ],
      },
    ],
  },
]

export const settingsMenu = [
  {
    route: '#',
    label: 'Pengaturan',
    subMenus: [
      {
        route: '/settings/appearance',
        label: 'Pengaturan Tampilan',
      },
      {
        route: '/settings/role',
        label: 'Pengaturan Role',
      },
      {
        route: '/settings/user',
        label: 'Pengaturan User',
      },
    ],
  },
]

export default menus
